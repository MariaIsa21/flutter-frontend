class Constants {
  static const String urlAuthority = 'http://127.0.0.1:8081';
  static const String urlFindAllClientes = '/api/cliente';
  static const String urlInsertCliente = '/api/cliente';
  static const String urlDeleteCliente = '/api/cliente';
  static const String urlListByIdCliente = '/api/cliente/';
  static const String urlFindAllAutos = '/api/autos';
  static const String urlListByIdAuto = '/api/autos/';
  static const String urlInsertAuto = '/api/autos';
  static const String urlDeleteAuto = '/api/autos';
  static const String urlFindAllSeguros = '/api/seguro';
  static const String urlListByIdSeguro = '/api/seguro/';
  static const String urlInsertSeguro = '/api/seguro';
  static const String urlDeleteSeguro = '/api/seguro';
  static const String urlFindAllVendedores = '/api/seguro';
  static const String urlListByIdVendedor = '/api/seguro/';
  static const String urlInsertVendedor = '/api/seguro';
  static const String urlDeleteVendedor = '/api/seguro';
}
