import 'dart:convert';
import 'dart:io';

import 'package:concesionario/models/apiresponse_model.dart';
import 'package:concesionario/models/errorapi_model.dart';
import 'package:concesionario/models/seguro_model.dart';
import 'package:concesionario/resource/Constants.dart';
import 'package:http/http.dart' as http;

class SeguroApiService {
  late Seguro _seguro;
  late ErrorApiResponse _error;
  SeguroApiService();

  Future<ApiResponse> getAllSeguros() async {
    List<Seguro> listSeguros = [];
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlFindAllSeguros);
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer " },
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listSeguros.add(Seguro.fromJson(i));
        return i;
      });
      apiResponse.object = listSeguros;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> listbyidseguro(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(
      Constants.urlAuthority,
      Constants.urlListByIdSeguro + id.toString(),
    );
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer " },
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _seguro = Seguro.fromJson(resBody);
      apiResponse.object = _seguro;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> insertSeguro(Seguro seguro) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(seguro.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlInsertSeguro);
    var res = await http.post(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: "application/json",
        HttpHeaders.authorizationHeader: "Bearer " ,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _seguro = Seguro.fromJson(resBody);
      apiResponse.object = _seguro;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> deleteSeguro(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(seguro.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlDeleteSeguro);
    var res = await http.delete(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: "application/json",
        HttpHeaders.authorizationHeader: "Bearer " ,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      apiResponse.message = resBody.toString();
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Seguro get seguro => _seguro;
}
