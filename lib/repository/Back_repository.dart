

import 'package:concesionario/models/apiresponse_model.dart';
import 'package:concesionario/models/auto_model.dart';
import 'package:concesionario/models/cliente_model.dart';
import 'package:concesionario/models/seguro_model.dart';
import 'package:concesionario/models/vendedor_model.dart';
import 'package:concesionario/repository/auto_Api_Service.dart';
import 'package:concesionario/repository/cliente_Api_Service.dart';
import 'package:concesionario/repository/seguro_Api_Service.dart';
import 'package:concesionario/repository/vendedor_Api_Service.dart';

class Backrepository {

      final clienteApiService = ClienteApiService();
      final seguroApiService = SeguroApiService();
      final autoApiService = AutoApiService();
      final vendedoresApiService = VendedorApiService();


      ///Cliente

      Future<ApiResponse> todoslosclientes() => clienteApiService.getAllClientes();

      Future<ApiResponse> clienteporid(int id) => clienteApiService.listbyidcliente(id);

      Future<ApiResponse> insertarcliente(Cliente cliente) => clienteApiService.insertCliente(cliente);

      Future<ApiResponse> borrarcliente(int id) => clienteApiService.deleteCliente(id);

      ///Seguro

      Future<ApiResponse> todoslosseguros() => seguroApiService.getAllSeguros();

      Future<ApiResponse> seguroporid(int id) => seguroApiService.listbyidseguro(id);

      Future<ApiResponse> insertarseguro(Seguro seguro) => seguroApiService.insertSeguro(seguro);

      Future<ApiResponse> borrarSeguro(int id) => seguroApiService.deleteSeguro(id);

      ///Autos

      Future<ApiResponse> todoslosautos() => autoApiService.getAllAutos();

      Future<ApiResponse> autoporid(int id) => autoApiService.listbyidauto(id);

      Future<ApiResponse> insertarauto(Auto auto) => autoApiService.insertAuto(auto);

      Future<ApiResponse> borrarauto(int id) => autoApiService.deleteAuto(id);

      ///Vendedores

      Future<ApiResponse> todoslosvendedores() =>  vendedoresApiService.getAllVendedores();

      Future<ApiResponse> vendedorporid(int id) => vendedoresApiService.listbyidvendedor(id);

      Future<ApiResponse> insertarvendedor(Vendedor vendedor) => vendedoresApiService.insertVendedor(vendedor);

      Future<ApiResponse> borrarvendedor(int id) => vendedoresApiService.deleteVendedor(id);

}