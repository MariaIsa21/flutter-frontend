import 'dart:convert';
import 'dart:io';
import 'package:concesionario/models/apiresponse_model.dart';
import 'package:concesionario/models/errorapi_model.dart';
import 'package:concesionario/models/auto_model.dart';
import 'package:concesionario/resource/Constants.dart';
import 'package:http/http.dart' as http;

class AutoApiService {
  late Auto _auto;
  late ErrorApiResponse _error;
  AutoApiService();

  Future<ApiResponse> getAllAutos() async {
    List<Auto> listAutos = [];
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlFindAllAutos);
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer " },
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listAutos.add(Auto.fromJson(i));
        return i;
      });
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> listbyidauto(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(
      Constants.urlAuthority,
      Constants.urlListByIdAuto + id.toString(),
    );
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer " },
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _auto = Auto.fromJson(resBody);
      apiResponse.object = _auto;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> insertAuto(Auto auto) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(auto.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlInsertAuto);
    var res = await http.post(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: "application/json",
        HttpHeaders.authorizationHeader: "Bearer " ,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _auto = Auto.fromJson(resBody);
      apiResponse.object = _auto;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> deleteAuto(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(auto.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlDeleteAuto);
    var res = await http.delete(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: "application/json",
        HttpHeaders.authorizationHeader: "Bearer " ,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      apiResponse.message = resBody.toString();
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Auto get auto => _auto;
}
