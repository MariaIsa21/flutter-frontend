import 'dart:convert';
import 'dart:io';
import 'package:concesionario/models/apiresponse_model.dart';
import 'package:concesionario/models/errorapi_model.dart';
import 'package:concesionario/models/cliente_model.dart';
import 'package:concesionario/resource/Constants.dart';
import 'package:http/http.dart' as http;

class ClienteApiService {
  late Cliente _cliente;
  late ErrorApiResponse _error;
  ClienteApiService();

  Future<ApiResponse> getAllClientes() async {
    List<Cliente> listClientes = [];
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlFindAllClientes);
    var res = await http.get(
      url,

    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listClientes.add(Cliente.fromJson(i));
        return i;
      });
      apiResponse.object = listClientes;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> listbyidcliente(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(
      Constants.urlAuthority,
      Constants.urlListByIdCliente + id.toString(),
    );
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer " },
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _cliente = Cliente.fromJson(resBody);
      apiResponse.object = _cliente;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> insertCliente(Cliente cliente) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(cliente.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlInsertCliente);
    var res = await http.post(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: "application/json",
        HttpHeaders.authorizationHeader: "Bearer " ,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _cliente = Cliente.fromJson(resBody);
      apiResponse.object = _cliente;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> deleteCliente(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(cliente.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlDeleteCliente);
    var res = await http.delete(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: "application/json",
        HttpHeaders.authorizationHeader: "Bearer "  ,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      apiResponse.message = resBody.toString();
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Cliente get cliente => _cliente;
}
