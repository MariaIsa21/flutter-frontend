import 'dart:convert';
import 'dart:io';
import 'package:concesionario/models/apiresponse_model.dart';
import 'package:concesionario/models/errorapi_model.dart';
import 'package:concesionario/models/vendedor_model.dart';
import 'package:concesionario/resource/Constants.dart';
import 'package:http/http.dart' as http;

class VendedorApiService {
  late Vendedor _vendedor;
  late ErrorApiResponse _error;
  VendedorApiService();

  Future<ApiResponse> getAllVendedores() async {
    List<Vendedor> listVendedores = [];
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlFindAllVendedores);
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer " },
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listVendedores.add(Vendedor.fromJson(i));
        return i;
      });
      apiResponse.object = listVendedores;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> listbyidvendedor(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(
      Constants.urlAuthority,
      Constants.urlListByIdVendedor + id.toString(),
    );
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer " },
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _vendedor = Vendedor.fromJson(resBody);
      apiResponse.object = _vendedor;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }
  Future<ApiResponse> insertVendedor(Vendedor vendedor) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(vendedor.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlInsertVendedor);
    var res = await http.post(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: "application/json",
        HttpHeaders.authorizationHeader: "Bearer " ,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _vendedor = Vendedor.fromJson(resBody);
      apiResponse.object = _vendedor;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> deleteVendedor(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(vendedor.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlDeleteVendedor);
    var res = await http.delete(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: "application/json",
        HttpHeaders.authorizationHeader: "Bearer " ,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      apiResponse.message = resBody.toString();
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Vendedor get vendedor => _vendedor;
}
