
import 'dart:async';



import 'package:concesionario/models/vendedor_model.dart';
import 'package:concesionario/repository/Back_repository.dart';
import 'package:concesionario/models/apiresponse_model.dart';
import 'package:flutter/cupertino.dart';

class VendedorBloc{
  late BuildContext _context;
  final _repository = Backrepository();
  var _apiResponse = ApiResponse();
  final _vendedorcontroller = StreamController<Vendedor>();

  Stream<Vendedor> get vendedor => _vendedorcontroller.stream;


  ApiResponse get apiResponse => _apiResponse;
  VendedorBloc(BuildContext context){
    _context = context ;
  }

  Future listarvendedores() async{
    var apiResponse = await _repository.todoslosvendedores();
  }


  Future listarvendedoresporid(int id) async{
    var apiResponse = await _repository.vendedorporid(id);
  }

  Future insertarseguros(Vendedor vendedor) async{
    var apiResponse = await _repository.insertarvendedor(vendedor);

  }

  Future borrarvendedores(int id) async{
    var apiResponse = await _repository.borrarvendedor(id);

  }


  dispose(){

    _vendedorcontroller.close();
  }


}