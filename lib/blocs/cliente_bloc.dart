
import 'dart:async';

import 'package:concesionario/models/cliente_model.dart';
import 'package:concesionario/repository/Back_repository.dart';
import 'package:concesionario/models/apiresponse_model.dart';
import 'package:flutter/cupertino.dart';

class ClienteBloc{
 late BuildContext _context;
 final _repository = Backrepository();
 var _apiResponse = ApiResponse();
 final _clientecontroller = StreamController<Cliente>();

 Stream<Cliente> get cliente => _clientecontroller.stream;


 ApiResponse get apiResponse => _apiResponse;
  ClienteBloc(BuildContext context){
 _context = context ;
  }

 Future listarclientes() async{
 var apiResponse = await _repository.todoslosclientes();
  }


 Future listarclientesid(int id) async{
  var apiResponse = await _repository.clienteporid(id);
 }

 Future insertarcliente(Cliente cliente) async{
   var apiResponse = await _repository.insertarcliente(cliente);

 }

 Future borrarcliente(int id) async{
  var apiResponse = await _repository.borrarcliente(id);

 }

 dispose(){

  _clientecontroller.close();
 }


}