
import 'dart:async';


import 'package:concesionario/models/seguro_model.dart';
import 'package:concesionario/repository/Back_repository.dart';
import 'package:concesionario/models/apiresponse_model.dart';
import 'package:flutter/cupertino.dart';

class SeguroBloc{
  late BuildContext _context;
  final _repository = Backrepository();
  var _apiResponse = ApiResponse();
  final _segurocontroller = StreamController<Seguro>();

  Stream<Seguro> get seguro => _segurocontroller.stream;


  ApiResponse get apiResponse => _apiResponse;
  SeguroBloc(BuildContext context){
    _context = context ;
  }

  Future listarseguros() async{
    var apiResponse = await _repository.todoslosseguros();
  }


  Future listarsegurosid(int id) async{
    var apiResponse = await _repository.seguroporid(id);
  }

  Future insertarseguros(Seguro seguro) async{
    var apiResponse = await _repository.insertarseguro(seguro);

  }

  Future borrarseguros(int id) async{
    var apiResponse = await _repository.borrarSeguro(id);

  }

  dispose(){

    _segurocontroller.close();
  }


}