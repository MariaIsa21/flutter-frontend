
import 'dart:async';

import 'package:concesionario/models/auto_model.dart';
import 'package:concesionario/repository/Back_repository.dart';
import 'package:concesionario/models/apiresponse_model.dart';
import 'package:flutter/cupertino.dart';

class AutoBloc{
  late BuildContext _context;
  final _repository = Backrepository();
  var _apiResponse = ApiResponse();
  final _autocontroller = StreamController<Auto>();

  Stream<Auto> get auto => _autocontroller.stream;


  ApiResponse get apiResponse => _apiResponse;
  AutoBloc(BuildContext context){
    _context = context ;
  }

  Future listarautos() async{
    var apiResponse = await _repository.todoslosautos();
  }


  Future listarautosid(int id) async{
    var apiResponse = await _repository.autoporid(id);
  }

  Future insertarauto(Auto auto) async{
    var apiResponse = await _repository.insertarauto(auto);

  }

  Future borrarauto(int id) async{
    var apiResponse = await _repository.borrarauto(id);

  }

dispose(){

  _autocontroller.close();
}

}