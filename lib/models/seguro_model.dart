import 'package:concesionario/models/auto_model.dart';
import 'package:concesionario/models/cliente_model.dart';

class Seguro {
  late int id;
  late Auto auto;
  late Cliente propietario;
  late DateTime fechaExpedicion, fechaVencimiento;

  Seguro({
    id,
    auto,
    propietario,
    fechaExpedicion,
    fechaVencimiento,
  });

  factory Seguro.fromJson(Map<String, dynamic> parsedJson) => Seguro(
        id: parsedJson['id_Seguro'],
        auto: parsedJson['id_Auto'],
        propietario: parsedJson['id_Propietario'],
        fechaExpedicion: parsedJson['fecha_Expedicion'],
        fechaVencimiento: parsedJson['fecha_Vencimiento'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'auto': auto.toJson(),
        'propietario': propietario.toJson(),
        'fechaExpedicion': fechaExpedicion,
        'fechaVencimiento': fechaVencimiento,
      };

  Map<String, dynamic> toJsonRegistry() => {
        'id': id,
      };
}
