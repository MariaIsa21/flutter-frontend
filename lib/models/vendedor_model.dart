class Vendedor {
  late int numeroDocumento, celular;
  late String tipoDocumento, nombreVendedor, direccionVendedor, ciudad;

  Vendedor({
    numeroDocumento,
    tipoDocumento,
    nombreVendedor,
    celular,
    direccionVendedor,
    ciudad,
  });

  factory Vendedor.fromJson(Map<String, dynamic> parsedJson) => Vendedor(
        numeroDocumento: parsedJson['numero_documento'],
        tipoDocumento: parsedJson['tipoDocumento'],
        nombreVendedor: parsedJson['nombreVendedor'],
        celular: parsedJson['celular'],
        direccionVendedor: parsedJson['direccionVendedor'],
        ciudad: parsedJson['ciudad'],
      );

  Map<String, dynamic> toJson() => {
        'numeroDocumento': numeroDocumento,
        'tipoDocumento': tipoDocumento,
        'nombreVendedor': tipoDocumento,
        'celular': celular,
        'direccionVendedor': direccionVendedor,
        'ciudad': ciudad,
      };

  Map<String, dynamic> toJsonRegistry() => {
        'numeroDocumento': numeroDocumento,
      };
}
