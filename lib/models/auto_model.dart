class Auto {
  late int id;
  late String marca, linea, tipoAuto, numeroChasis;

  Auto({
    id,
    marca,
    linea,
    tipoAuto,
    numeroChasis,
  });

  factory Auto.fromJson(Map<String, dynamic> parsedJson) => Auto(
        id: parsedJson['id_Auto'],
        marca: parsedJson['marca'],
        linea: parsedJson['linea'],
        tipoAuto: parsedJson['tipo_Auto'],
        numeroChasis: parsedJson['nro_Chasis'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'marca': marca,
        'linea': linea,
        'tipoAuto': tipoAuto,
        'numeroChasis': numeroChasis,
      };

  Map<String, dynamic> toJsonRegistry() => {
        'id': id,
      };
}
