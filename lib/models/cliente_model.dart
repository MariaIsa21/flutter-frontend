class Cliente {
  late int numeroDocumento, celular;
  late String tipoDocumento, nombreCliente, direccionCliente, ciudad;

  Cliente({
    numeroDocumento,
    tipoDocumento,
    nombreCliente,
    celular,
    direccionCliente,
    ciudad,
  });




  factory Cliente.fromJson(Map<String, dynamic> parsedJson) => Cliente(
        numeroDocumento: parsedJson['numero_Documento'],
        tipoDocumento: parsedJson['tipo_Documento'],
        nombreCliente: parsedJson['nombre_Cliente'],
        celular: parsedJson['celular'],
        direccionCliente: parsedJson['direccion_Cliente'],
        ciudad: parsedJson['ciudad'],
      );

  Map<String, dynamic> toJson() => {
        'numeroDocumento': numeroDocumento,
        'tipoDocumento': tipoDocumento,
        'nombreCliente': nombreCliente,
        'celular': celular,
        'direccionCliente': direccionCliente,
        'ciudad': ciudad,
      };

  Map<String, dynamic> toJsonRegistry() => {
        'numeroDocumento': numeroDocumento,
      };
}
